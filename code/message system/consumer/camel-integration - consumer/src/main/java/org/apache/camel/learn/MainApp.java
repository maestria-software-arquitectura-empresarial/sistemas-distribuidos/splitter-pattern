package org.apache.camel.learn;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.main.Main;
import org.apache.camel.component.http.HttpMethods;
/**
 * A Camel Application
 */
public class MainApp {
    public static void main(String[] args) throws Exception {
        CamelContext context = new DefaultCamelContext();

        // Configura la ruta Camel para recibir mensajes de ActiveMQ
        context.addRoutes(new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                from("activemq:queue:myqueue")  
                    .log("Received message: ${body}")
                    .setHeader("CamelHttpMethod", constant(HttpMethods.POST))
                    .setHeader("Content-Type", constant("application/json"))
                    .to("http://127.0.0.1:5000/enviar_json"); ;
            }
        });

        // Inicia el contexto de Camel para comenzar a recibir mensajes
        context.start();

        // Espera indefinidamente para mantener la aplicación en ejecución
        Thread.sleep(Long.MAX_VALUE);

        // Detiene el contexto de Camel cuando la aplicación se cierra
        context.stop();
    }
    /*
    public static void main(String[] args) throws Exception {
        Main main = new Main();
        CamelContext context = new DefaultCamelContext();

             context.addRoutes(new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                from("activemq:queue:myqueue")  
                    .log("Received message: ${body}")  ;
            }
        });
        main.run();
    }
*/
}

