package org.apache.camel.learn;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.main.Main;

/**
 * A Camel Application
 */
public class MainApp {

    /**
     * A main() so we can easily run these routing rules in our IDE
     */
/*    public static void main(String... args) throws Exception {
        Main main = new Main();
        main.configure().addRoutesBuilder(new MyRouteBuilder());
        main.run(args);
    }*/
    public static void main(String[] args) throws Exception {
        CamelContext context = new DefaultCamelContext();

        // Configura la ruta Camel para enviar mensajes a ActiveMQ
        context.addRoutes(new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                from("direct:start")  // Endpoint directo para enviar mensajes
                .to("activemq:queue:myqueue");  // Envia mensajes a la cola 'myqueue' en ActiveMQ
            }
        });

        // Inicia el contexto de Camel
        context.start();

        // Crea un productor para enviar mensajes
        String message = "{\"event_id\": \"123456789\",\"timestamp\": \"2024-04-05T10:15:30Z\",\"transaction_type\": \"debit\",\"amount\": 100.00,\"currency\": \"USD\",\"source_account\": \"1234567890\",\"destination_account\": \"0987654321\",\"description\": \"Transferencia por pago de servicios\",\"status\": \"completed\"}";
        context.createProducerTemplate().sendBody("direct:start", message);

        // Detiene el contexto de Camel
        context.stop();
    }

}

