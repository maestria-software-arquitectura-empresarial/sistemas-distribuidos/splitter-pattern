from flask import Flask, request, jsonify
import datetime
import platform
from threading import Thread
import requests

app = Flask(__name__)

# Lista de URLs de servicios a los que enviar el JSON
servicios_urls = [
    'http://127.0.0.1:8001/enviarMonitoreo',
    'http://127.0.0.1:8000/enviarNotificacion'
    ,'http://127.0.0.1:8005/enviarAi'
]

# Función para enriquecer el JSON con información adicional
def enriquecer_json(data):
    data['hora_procesamiento'] = datetime.datetime.now().isoformat()
    data['sistema_operativo'] = platform.system()
    data['arquitectura'] = platform.architecture()[0]
    data['nombre_equipo'] = platform.node()
    return data

# Función para enviar el JSON a los servicios de manera asíncrona
def enviar_json_async(data, servicio_url, results):
    try:
        response = requests.post(servicio_url, json=data, timeout=3)
        response.raise_for_status()
    except requests.exceptions.RequestException as e:
        print(e)
        results.append(False)
    else:
        results.append(True)

# Ruta para enviar el JSON recibido
@app.route('/enviar_json', methods=['POST'])
def enviar_json():
    data_noenriquecido = request.get_json()
    print('DATA NO ENRIQUECIDA %s' % data_noenriquecido)
    data_enriquecido = enriquecer_json(data_noenriquecido)
    print('DATA ENRIQUECIDA %s' % data_enriquecido)

    # Iniciar un hilo para enviar el JSON a cada servicio de manera asíncrona
    threads = []
    results = []
    for servicio_url in servicios_urls:
        thread = Thread(target=enviar_json_async, args=(data_enriquecido, servicio_url, results))
        thread.start()
        threads.append(thread)

    # Esperar a que todos los hilos terminen
    for thread in threads:
        thread.join()

    # Determinar el mensaje y código de estado en función de los resultados
    mensaje = {"code":"000","message": "JSON procesado", "tech":"JSON procesado por todos los servicios"}
    status_code = 200
    if False in results:
        mensaje = {"code":"000","message": "JSON procesado", "tech":"Peticion procesada, existio problemas envio en algunos servicios, se procesa reintento"}
        status_code = 500

    return jsonify(mensaje), status_code

if __name__ == '__main__':
    app.run(port=5000)
