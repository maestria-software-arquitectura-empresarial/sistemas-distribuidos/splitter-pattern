import time
import sys
import stomp

class Listener(stomp.ConnectionListener):
    def on_error(self, headers, message):
        print('received an error "%s"' % message)
    
    def on_message(self, headers, message):
        print('received a message "%s"' % message)

# Create a connection to the ActiveMQ broker
# conn = stomp.Connection([('localhost', 61616)])
conn = stomp.Connection([('localhost', 61616)], heartbeats=(4000, 4000))

conn.set_listener('', Listener())
conn.connect(username='admin', passcode='admin', wait=True)

# Subscribe to the topic
conn.subscribe(destination='/topic/topic-1', id=1, ack='auto')

# Send a message to the topic
conn.send(body=' '.join(sys.argv[1:]), destination='/topic/topic-1')

# Wait for a while before disconnecting
time.sleep(2)
conn.disconnect()
