from flask import Flask, request, jsonify

app = Flask(__name__)

datos_en_memoria = []

@app.route('/enviarNotificacion', methods=['POST'])
def almacenar_json():
    data = request.get_json()
    datos_en_memoria.append(data)
    return jsonify({"code":"000","message": "Notificacion procesandose"}), 200

@app.route('/consultar_datos', methods=['GET'])
def consultar_datos():
    return jsonify({"datos": datos_en_memoria}), 200

if __name__ == '__main__':
    app.run(port=8000)
