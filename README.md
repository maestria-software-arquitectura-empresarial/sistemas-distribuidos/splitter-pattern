# SPLITTER PATTERN ON EVENT DRIVEN ARCHITECTURE

DIAGRAMA:
![diagrama splitter pattern](Diagrama.drawio.png)


PASOS PARA CORRER PROYECTO:

Ejecutar:
```

docker pull webcenter/activemq
docker run -d --name activemq -p 61616:61616 -p 8161:8161 webcenter/activemq
```

EN CARPETA code\splitter EJECUTAR:
```

python .\splitter.py
python .\ai.py
python .\notificaciones.py
python .\monitoreo.py
```

EN CARPETA "code\message system\consumer\camel-integration" - consumer Y "code\message system\producer\camel-integration" EJECUTAR:
```
mvn camel:run
```
